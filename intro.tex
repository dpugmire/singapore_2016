\section*{Introduction}
\label{sec:intro}

The cost of data movement is quickly becoming a bottleneck to computation~\cite{Ahern2011}. Recently deployed supercomputers, and those planned for the future
provide far more computational capacity than I/O bandwidth. Further, the
supercomputers being proposed show little, to no increases in I/O bandwidth. This will
prove to be an increasing challenge to extract knowledge from larger,
and more quickly generated volumes of data.

Traditionally, a simulation running on a supercomputer will write output data
to disk within a parallel file system. Visualization of these data are performed
as a post-processing task. The output data are read from disk into the memory
of a parallel tool for analysis and visualization.

Visualization is generally I/O bound~\cite{Childs2010,HPVChapter13}, and as the relative I/O bandwidth continues to decrease, the challenges of visualization of increasingly larger data will become more problematic. In the case of traditional visualization, the I/O bottleneck is exacerbated as data are first written to disk by the simulation, and then read back from disk by the visualization routine. Thus visualization and analysis
is subject to the I/O bottleneck from the producer, as well as a consumer.

To help alleviate these issues, the visualization community has turned to in situ 
processing paradigms~\cite{HPVChapter9}. These techniques aim to minimize the
movement of data and perform computations, such as visualization and analyis,
before the data are written to disk. By avoiding the I/O bottleneck, the cost
of processing, analyzing, and visualizing the data are dramatically reduced.

Broadly speaking, two in situ processing paradigms have emerged.
First, {\em co-processing}, or
{\em tightly-coupled} methods. In this model,
the visualization code runs
within the simulation. After each iteration in the simulation,
routines are executed on the simulation nodes using the data in
memory to preform the specific analyses and visualizations.
Second, {\em concurrent-processing}, or
{\em loosely-coupled} methods.
In this model, the simulation code transfers data over the network to
another set of resources where the visualization routines are
executed. In this model, the visualization is done asyncronously from
the simulation, and in a separate memory space.
And finally, {\em hybrid} methods~\cite{HPVChapter9} combine
elements of both co-processing and concurrent-processing. For example, the data are
processed on the simulation resource and then sent to a separate
resource for further processing.

An additional barrier is the dramatic changes occuring in supercomputer architectures. For years,
basic architectures for supercomputers have remained relatively consistent.
However, revolutionary changes are appearing on leading edge supercomputers.
Currently planned supercomputers will have even more revolutionary architectures.
One of the most significant changes in these emerging architectures is the enormous
increase in parallelism per node. Accelerators, such as GPUs,
can support many thousands of threads of execution. GPUs, and other accelerators planned for the future
will support increasingly more parallelism.

The increases in on-node parallelism does not match well with the current HPC codes
in use, including visualization algorithms. The new processor architectures require
new programming models and algorithmic approaches. The re-design of algorithms
for these new architectures poses a critical
challenge going forward~\cite{Childs:2013}.

Managing these complexities requires careful handling of data movement and operations on data that can be performed while the data are being transfered through the system. To better understand these complexities, we have designed and tested a system which reads and performs visualization operations on simulation data as it is being moved within the workflow.  This system is built on top of two different research projects, ADIOS~\cite{HelloADIOS}, and VTKm~\cite{Moreland:2015}.
ADIOS is a richly-featured middleware layer designed to efficiently handle the movement of large amounts of data and used in large number of production simulation codes
in supercomputing centers.  VTKm is a new framework for efficiently representing, and operating on data in high-concurrency environments. VTKm is a fusion of three
independant research projects, EAVL~\cite{meredith2012eavl}, DAX~\cite{DAX},
and Piston~\cite{Piston}.

In this paper, we describe and explore a workflow system we have designed, built and tested which explores light-weight VTKm visualization operations that have been deployed as plugins in the ADIOS in-transit data staging framework.  We are particularly interested in the usability, performance and scaling of these operations in a production environment, and how this system can scale up to larger and larger workflows.


%% \begin{itemize}
%% \item What's the problem?
%% \begin{itemize}
%% \item big data and simulations/experiments
%% \item Interact with variety of data types from a variety of sources
%% \item Perform operations on diverse hardware, and memory footprints, etc.
%% \item Workflow system for dealing with these
%% \end{itemize}
%% \end{itemize}

%% big data. ADIOS. EAVL. Frameworks for doing in situ stuff.
