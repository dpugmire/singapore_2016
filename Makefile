all:
	pdflatex paper.tex
	bibtex paper.aux
	pdflatex paper.tex

clean:
	rm *.pdf *.log *.aux

